from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from .swagger_settings import swagger_urlpatterns
urlpatterns = [
    path('admin/', admin.site.urls),
    path('tasks/', include('tasks.urls')),

    *swagger_urlpatterns,
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
