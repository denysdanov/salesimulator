from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView
from .serializers import TestSerialiser, TestPictureSerialiser, QuestionSerialiser
from .models import Test, Question


class TestView(ListCreateAPIView):
    serializer_class = TestSerialiser
    queryset = Test.objects.all()


class TestDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = TestSerialiser
    queryset = Test.objects.all()

    def get_object(self):
        return Test.objects.get(id=self.kwargs['pk'])


class LoadPictureView(CreateAPIView):
    serializer_class = TestPictureSerialiser


class QuestionView(ListCreateAPIView):
    serializer_class = QuestionSerialiser
    queryset = Question.objects.all()


class QuestionDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = QuestionSerialiser
    queryset = Question.objects.all()

    def get_object(self):
        return Question.objects.get(id=self.kwargs['pk'])