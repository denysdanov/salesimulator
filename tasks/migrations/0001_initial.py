# Generated by Django 4.1.4 on 2022-12-28 12:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description')),
            ],
        ),
        migrations.CreateModel(
            name='TestPicture',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('picture', models.ImageField(upload_to='', verbose_name='Picture')),
                ('test', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='picture', to='tasks.test')),
            ],
        ),
        migrations.CreateModel(
            name='TestIntroductoryVideo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.URLField(verbose_name='Introductory video')),
                ('test', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='introductory_video', to='tasks.test')),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Name')),
                ('question', models.URLField(verbose_name='Question')),
                ('answer', models.URLField(verbose_name='Right answer')),
                ('test', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='question', to='tasks.test')),
            ],
        ),
    ]
