from django.urls import path
from .views import *

urlpatterns = [
    path('', TestView.as_view()),
    path('<int:pk>/', TestDetailView.as_view()),
    path('load_picture/', LoadPictureView.as_view()),
    path('question/', QuestionView.as_view()),
    path('question/<int:pk>/', QuestionView.as_view()),
]