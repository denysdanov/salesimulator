from django.db.models import *
from django.utils.translation import gettext_lazy as _

class Test(Model):
    name = CharField(_("Name"), max_length = 64)
    description = TextField(_("Description"))
    test_introductory_video_link = URLField(_("Introductory video"))

class TestPicture(Model):
    test = ForeignKey(to=Test, on_delete=CASCADE, related_name='pictures')
    picture = ImageField(_("Picture"), upload_to='test_pictures')

    def __str__(self) -> str:
        return f'{self.picture.path}'
    
class Question(Model):
    test = ForeignKey(to=Test, on_delete=CASCADE, related_name='questions')
    name = CharField(_("Name"), max_length = 64)
    question = URLField(_("Question"))
    answer = URLField(_("Right answer"))