from rest_framework.serializers import ModelSerializer, StringRelatedField

from .models import Test, Question,  TestPicture


class QuestionSerialiser(ModelSerializer):
    class Meta:
        model = Question
        exclude = []

class TestPictureSerialiser(ModelSerializer):
    class Meta:
        model = TestPicture
        exclude = []

class TestSerialiser(ModelSerializer):
    pictures = TestPictureSerialiser(many=True)
    questions = QuestionSerialiser(many=True)
    class Meta:
        model = Test
        exclude = []